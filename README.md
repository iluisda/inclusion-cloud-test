# Incluion.Cloud Test
## Webpack 3 + SASS + Bootstrap 3.3.7 + Font Awesome
Proyecto realizado para Inclusion Cloud como test challenge frontend.

Usando namespace objects para encapsular funciones (Module Pattern)

Inicialmente, si se instala el proyecto local, mediante expres y nodemon levanta una api con la data que se aloja en ```/api``` 
port 8080, es decir levantará un servidor local localhost:8080/api. Dentro de el se encuentra un endpoint con la data de los usuarios.

Dentro de la url de la demo llama una data local _data.json que contiene lo misma data con diferentes IDs, empaquetada con Webpack.

La demo la encuentran acontinuación...
<br>
## DEMO

[Inclusion Test](http://inclusion.cloud-test.surge.sh/)

<br>

## Install dependencies

```
npm install
```


## Develop locally with webpack-dev-server && nodemon for api
1. Run

```
npm run devAll
```

2. In your browser, navigate to: [http://localhost:3000/](http://localhost:3000/)
## For bundled output

```
npm run build
```

## For production-ready output

```
npm run build:prod
```

## Loaders and Plugins used in this boilerplate

### Loaders
* babel-loader
* html-loader
* sass-loader
* css-loader
* style-loader
* file-loader
* url-loader

### Plugins
* clean-webpack-plugin
* extract-text-webpack-plugin
* html-webpack-plugin
