const express = require("express");

const app = express();

app.use(express.static("dist"));
app.get('/api/getUsers', function (req, res, next) {
    res.status(200).json({"users": 
        [
            {
                "ID":"1", 
                "Nombre":"Lucas", 
                "Apellido":"Gaitan",
                "DNI":"38762511"
            },
            {
                "ID":"2", 
                "Nombre":"Pepe", 
                "Apellido":"Gomez",
                "DNI":"213214124"
            },
            {
                "ID":"3", 
                "Nombre":"Guille", 
                "Apellido":"Grillo",
                "DNI":"42141251"
            }
        ]    
    });
});
app.listen(8080, () => console.log("Listening on port 8080!"));