import __sharedModule from './__sharedModule';
import _data from './_data.json';
$(document).ready(function () {
    var users;
    const location = window.location.hostname;
    var localData = _data;
    var getUsers = function () {
        if(location === 'localhost') {
            sharedModule.getUsers().then(result => {
                users = result.users.map(function (result) {
                    return result;
                });
                sharedModule.applyTemplate(users);
            });
        } else {
            users = localData.users.map(function (result) {
                return result;
            });
            sharedModule.applyTemplate(users);
        }
    }
    getUsers();
    //listen input search
    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#usuarios tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    //Refresh
    $('#refresh').on('click', function () {
        getUsers();
    });

    //Order 
    $("#orderBy").change(function () {
        var value = $(this).val();
        if (!!value)
            users.sort(sharedModule.dynamicSort(value));
        sharedModule.applyTemplate(users);
    })
})