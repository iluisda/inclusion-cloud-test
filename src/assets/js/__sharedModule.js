
$(document).ready(function () {
  var sharedModule;
  const url = "/api";

  sharedModule = {

    getUsers: function () {
      return $.getJSON(url + "/getUsers", function (data) {
          return data;
        })
        .done(function () {
          console.log("second success");
        })
        .fail(function () {
          console.log("error");
        })
    },
    applyTemplate: function (data) {
      $('#usuarios').empty();
      for (var i in data) {
        $('#usuarios').append("<tr>" +
          "<td>" + data[i].ID + "</td>" +
          "<td>" + data[i].Nombre + "</td>" +
          "<td>" + data[i].Apellido + "</td>" +
          "<td>" + data[i].DNI + "</td>" +
          "</tr>"
        );
      };
    },
    searchUser: function (value) {
      return new Promise(function (resolve, reject) {
        setTimeout(function () {
          resolve(value);
        }, 100);
      })
    },
    dynamicSort: function (property) {
      var sortOrder = 1;

      if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
      }

      return function (a, b) {
        if (sortOrder == -1) {
          return b[property].localeCompare(a[property]);
        } else {
          return a[property].localeCompare(b[property]);
        }
      }
    }
  };

  window.sharedModule = sharedModule;
});